/*
 * This file is generated by jOOQ.
 */
package be.lghs.accounting.model.tables.records;


import be.lghs.accounting.model.tables.MovementTags;

import java.util.UUID;

import javax.annotation.processing.Generated;

import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.14.9",
        "schema version:accounting_0010"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class MovementTagsRecord extends UpdatableRecordImpl<MovementTagsRecord> implements Record2<UUID, String> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>accounting.movement_tags.movement_id</code>.
     */
    public void setMovementId(UUID value) {
        set(0, value);
    }

    /**
     * Getter for <code>accounting.movement_tags.movement_id</code>.
     */
    public UUID getMovementId() {
        return (UUID) get(0);
    }

    /**
     * Setter for <code>accounting.movement_tags.content</code>.
     */
    public void setContent(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>accounting.movement_tags.content</code>.
     */
    public String getContent() {
        return (String) get(1);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record2<UUID, String> key() {
        return (Record2) super.key();
    }

    // -------------------------------------------------------------------------
    // Record2 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row2<UUID, String> fieldsRow() {
        return (Row2) super.fieldsRow();
    }

    @Override
    public Row2<UUID, String> valuesRow() {
        return (Row2) super.valuesRow();
    }

    @Override
    public Field<UUID> field1() {
        return MovementTags.MOVEMENT_TAGS.MOVEMENT_ID;
    }

    @Override
    public Field<String> field2() {
        return MovementTags.MOVEMENT_TAGS.CONTENT;
    }

    @Override
    public UUID component1() {
        return getMovementId();
    }

    @Override
    public String component2() {
        return getContent();
    }

    @Override
    public UUID value1() {
        return getMovementId();
    }

    @Override
    public String value2() {
        return getContent();
    }

    @Override
    public MovementTagsRecord value1(UUID value) {
        setMovementId(value);
        return this;
    }

    @Override
    public MovementTagsRecord value2(String value) {
        setContent(value);
        return this;
    }

    @Override
    public MovementTagsRecord values(UUID value1, String value2) {
        value1(value1);
        value2(value2);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached MovementTagsRecord
     */
    public MovementTagsRecord() {
        super(MovementTags.MOVEMENT_TAGS);
    }

    /**
     * Create a detached, initialised MovementTagsRecord
     */
    public MovementTagsRecord(UUID movementId, String content) {
        super(MovementTags.MOVEMENT_TAGS);

        setMovementId(movementId);
        setContent(content);
    }
}
