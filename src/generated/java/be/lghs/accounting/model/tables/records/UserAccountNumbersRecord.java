/*
 * This file is generated by jOOQ.
 */
package be.lghs.accounting.model.tables.records;


import be.lghs.accounting.model.tables.UserAccountNumbers;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.annotation.processing.Generated;

import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.14.9",
        "schema version:accounting_0010"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UserAccountNumbersRecord extends UpdatableRecordImpl<UserAccountNumbersRecord> implements Record4<UUID, String, Boolean, LocalDateTime> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>accounting.user_account_numbers.user_id</code>.
     */
    public void setUserId(UUID value) {
        set(0, value);
    }

    /**
     * Getter for <code>accounting.user_account_numbers.user_id</code>.
     */
    public UUID getUserId() {
        return (UUID) get(0);
    }

    /**
     * Setter for <code>accounting.user_account_numbers.account_number</code>.
     */
    public void setAccountNumber(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>accounting.user_account_numbers.account_number</code>.
     */
    public String getAccountNumber() {
        return (String) get(1);
    }

    /**
     * Setter for <code>accounting.user_account_numbers.validated</code>.
     */
    public void setValidated(Boolean value) {
        set(2, value);
    }

    /**
     * Getter for <code>accounting.user_account_numbers.validated</code>.
     */
    public Boolean getValidated() {
        return (Boolean) get(2);
    }

    /**
     * Setter for <code>accounting.user_account_numbers.encoding_date</code>.
     */
    public void setEncodingDate(LocalDateTime value) {
        set(3, value);
    }

    /**
     * Getter for <code>accounting.user_account_numbers.encoding_date</code>.
     */
    public LocalDateTime getEncodingDate() {
        return (LocalDateTime) get(3);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record2<UUID, String> key() {
        return (Record2) super.key();
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row4<UUID, String, Boolean, LocalDateTime> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    @Override
    public Row4<UUID, String, Boolean, LocalDateTime> valuesRow() {
        return (Row4) super.valuesRow();
    }

    @Override
    public Field<UUID> field1() {
        return UserAccountNumbers.USER_ACCOUNT_NUMBERS.USER_ID;
    }

    @Override
    public Field<String> field2() {
        return UserAccountNumbers.USER_ACCOUNT_NUMBERS.ACCOUNT_NUMBER;
    }

    @Override
    public Field<Boolean> field3() {
        return UserAccountNumbers.USER_ACCOUNT_NUMBERS.VALIDATED;
    }

    @Override
    public Field<LocalDateTime> field4() {
        return UserAccountNumbers.USER_ACCOUNT_NUMBERS.ENCODING_DATE;
    }

    @Override
    public UUID component1() {
        return getUserId();
    }

    @Override
    public String component2() {
        return getAccountNumber();
    }

    @Override
    public Boolean component3() {
        return getValidated();
    }

    @Override
    public LocalDateTime component4() {
        return getEncodingDate();
    }

    @Override
    public UUID value1() {
        return getUserId();
    }

    @Override
    public String value2() {
        return getAccountNumber();
    }

    @Override
    public Boolean value3() {
        return getValidated();
    }

    @Override
    public LocalDateTime value4() {
        return getEncodingDate();
    }

    @Override
    public UserAccountNumbersRecord value1(UUID value) {
        setUserId(value);
        return this;
    }

    @Override
    public UserAccountNumbersRecord value2(String value) {
        setAccountNumber(value);
        return this;
    }

    @Override
    public UserAccountNumbersRecord value3(Boolean value) {
        setValidated(value);
        return this;
    }

    @Override
    public UserAccountNumbersRecord value4(LocalDateTime value) {
        setEncodingDate(value);
        return this;
    }

    @Override
    public UserAccountNumbersRecord values(UUID value1, String value2, Boolean value3, LocalDateTime value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached UserAccountNumbersRecord
     */
    public UserAccountNumbersRecord() {
        super(UserAccountNumbers.USER_ACCOUNT_NUMBERS);
    }

    /**
     * Create a detached, initialised UserAccountNumbersRecord
     */
    public UserAccountNumbersRecord(UUID userId, String accountNumber, Boolean validated, LocalDateTime encodingDate) {
        super(UserAccountNumbers.USER_ACCOUNT_NUMBERS);

        setUserId(userId);
        setAccountNumber(accountNumber);
        setValidated(validated);
        setEncodingDate(encodingDate);
    }
}
