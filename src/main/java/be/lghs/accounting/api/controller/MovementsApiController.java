package be.lghs.accounting.api.controller;

import be.lghs.accounting.api.generated.controller.MovementsApi;
import be.lghs.accounting.api.generated.model.Movement;
import be.lghs.accounting.configuration.Roles;
import be.lghs.accounting.repositories.MovementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Controller
@RequestMapping("${openapi.lghsAccounting.base-path:/api}")
@RequiredArgsConstructor
public class MovementsApiController implements MovementsApi {

    private final MovementRepository movementRepository;

    @Override
    @Transactional(readOnly = true)
    @Secured(Roles.ROLE_TREASURER)
    public ResponseEntity<List<Movement>> allMovements() {
        var categoryNamesById = movementRepository.categoryNamesById();

        return ResponseEntity.of(Optional.of(movementRepository.findAll()
                .stream()
                .map(m ->
                new Movement()
                        .id(m.getId().toString())

                        .from(m.getCounterPartyName())
                        .category(Optional.ofNullable(m.getCategoryId()).map(c -> categoryNamesById.getOrDefault(c, c.toString())).orElse(null))
                        .communication(m.getCommunication())
                        .amount(m.getAmount().multiply(BigDecimal.valueOf(100)).intValue())
                        .entryDate(m.getEntryDate())

        ).collect(Collectors.toList())));
    }

}
