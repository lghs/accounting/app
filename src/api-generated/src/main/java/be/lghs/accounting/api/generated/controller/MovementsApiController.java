package be.lghs.accounting.api.generated.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;
import java.util.Optional;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-09T15:10:45.039Z[GMT]")
@Controller
@RequestMapping("${openapi.lghsAccounting.base-path:/api}")
public class MovementsApiController implements MovementsApi {

    private final NativeWebRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public MovementsApiController(NativeWebRequest request) {
        this.request = request;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

}
