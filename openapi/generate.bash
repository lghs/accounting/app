#!/bin/bash

set -ue
set -o pipefail

DIR=$(cd $(dirname $0) && pwd)

INFO_COLOR='\033[0;33m'
ERROR_COLOR='\033[0;3om'
NC='\033[0m' # No Color

info(){
  echo -e ${INFO_COLOR}$@${NC}
}

error(){
  >&2 echo -e ${ERROR_COLOR}$@${NC}
}

GENERATOR_VERSION=v5.1.0

info generage backend
rm -rf $DIR/../src/api-generated
docker run --rm -u $(id -u) -w /local -v "$DIR/../:/local" openapitools/openapi-generator-cli:$GENERATOR_VERSION generate \
    -i api.yaml \
    -g spring \
    -o src/api-generated \
    -c openapi/back/config.yaml

rm $DIR/../src/api-generated/src/main/java/org/openapitools/OpenAPI2SpringBoot.java
