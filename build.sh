#!/bin/sh

set -ue

DIR=$(cd $(dirname $0) && pwd)

INFO_COLOR='\033[0;33m'
ERROR_COLOR='\033[0;3om'
NC='\033[0m' # No Color

info(){
  echo -e ${INFO_COLOR}$@${NC}
}

error(){
  >&2 echo -e ${ERROR_COLOR}$@${NC}
}

IMAGE_PREFIX=$1
IMAGE_TAG=$2

info build with image prefix $IMAGE_PREFIX and tag $IMAGE_TAG


info build front
docker-compose -f $DIR/dev-env/docker-compose.yml run app gradle bootJar

rm -rf $DIR/tmp || echo ""
mkdir -p  $DIR/tmp
cp $DIR/build/libs/src-*.jar $DIR/tmp/boot.jar
cp $DIR/Dockerfile $DIR/tmp/Dockerfile
docker build -t $IMAGE_PREFIX$IMAGE_TAG $DIR/tmp
