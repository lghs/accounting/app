create user lghs_accounting_root with password 'lghs_accounting_root_password'; -- change it really
create database lghs_accounting owner lghs_accounting_root;

\c lghs_accounting
revoke all on schema public from public;

create user lghs_accounting_app with password 'lghs_accounting_app_password'; -- change it really
grant connect on database lghs_accounting to lghs_accounting_app;

grant usage on schema public to lghs_accounting_root;
grant usage on schema public to lghs_accounting_app;

create extension "uuid-ossp";
create extension pgcrypto;
create extension btree_gist;
