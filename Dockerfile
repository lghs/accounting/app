FROM registry.gitlab.com/lghs/accounting/accounting-build-docker


COPY ./boot.jar /boot.jar

ENTRYPOINT ["java","-jar","/boot.jar"]


